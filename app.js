

process.env.NODE_ENV = 'localDevelopment';
config = require('config');
dbConnection = require('./routes/dbConnection');
constant = require('./routes/constant');


var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');

var app = express();


var routes = require('./routes/index');
var users = require('./routes/users');

//mailer
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'shashank.sharma@theroboticshomepage.com',
        pass: 'jugnoo123'
    }
});
var mailOptions = {
    from: 'Shashank Sharma  <shashank.sharma@mail.click-labs.com>', // sender address
    to: 'shubham.kaushal@mail.click-labs.com', // list of receivers
    subject: 'projects', // Subject line
    text: 'Done ?', // plaintext body
    attachments:[{   // use URL as an attachment
        filename: 'license.txt',
        path: 'https://raw.github.com/andris9/Nodemailer/master/LICENSE'
    }],
    html: '<b>hi Shubham </b>' // html body
};

// send mail with defined transport object
transporter.sendMail(mailOptions, function(error, info){
    if(error){
        return console.log(error);
    }
    console.log('Message sent: ' + info.response);

});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/test', routes);
app.use('/users', users);
app.post('/register_user', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});

//module.exports = app;
